const os = require('os');
const osu = require('node-os-utils');
const mem = osu.mem;
const cpu = osu.cpu;


const mc = require('@mindconnect/mindconnect-nodejs');


async function getAgent() {
  const configuration = require("./agentconfig.json");
  const agent = new mc.MindConnectAgent(configuration);

  if (!agent.IsOnBoarded()) {
    await agent.OnBoard();
  }

  if (!agent.HasDataSourceConfiguration()) {
    await agent.GetDataSourceConfiguration();
  }
  
  return agent;
}

function createDataPoint(id, value) {
  return {
    dataPointId: id,
  qualityCode: "0",
  value: "" + value
  };
}

async function collectData() {
  const totalMem = os.totalmem()/1024/1024;
  const freeMem = os.freemem()/1024/1024;

  return {
    cpu_count: cpu.count(),
    cpu_available: await cpu.free(),
    cpu_usage: await cpu.usage(),
    mem_total: Math.round(totalMem),
    mem_used: Math.round(totalMem - freeMem),
    mem_free: Math.round(freeMem)
  };
}

function createDataPointSet(data, dataPointMapping) {
  const result = [];
  for (let key in data) {
    result.push(createDataPoint(dataPointMapping[key], data[key]));
  }
  return result;
}

async function update(agent, dataPointMapping) {
  const data = await collectData();
  console.log("Collected Data: ", data);
  const result = await agent.PostData(createDataPointSet(data, dataPointMapping));
  console.log("Data sent: ", data);
}

async function loop(agent, dataPointMapping) {
  while (true) {
    try {
      await update(agent, dataPointMapping);
    } catch (e) {
      console.log("An error occured:", e);
    }
  }
}

getAgent().then(agent => {
  const dataPointSources = agent._configuration.dataSourceConfiguration.dataSources[0].dataPoints;
  const dataPointMapping = {};
  for (let i = 0; i < dataPointSources.length; ++i) {
    dataPointMapping[dataPointSources[i].name] = dataPointSources[i].id;
  }
  console.log("Data Point Mapping: ", dataPointMapping);

  loop(agent, dataPointMapping).then(() => {}).catch(err => {
    console.log("An error occured!", err);
  });

}).catch(error => {
  console.log(error);
});
