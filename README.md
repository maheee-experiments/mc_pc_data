## How to run

* create asset of type ```core.mclib``` with the variables listed below
* download onboarding key and place as ```agentconfig.json``` in project folder
* run ```npm install```
* run ```npm run start```

### Variables

| Name | Type | Unit |
| ------ | ------ | ------ |
| mem_used | MB | INT |
| mem_total | MB | INT |
| mem_free | MB | INT |
| cpu_usage | % | DOUBLE |
| cpu_available | % | DOUBLE |
| cpu_count | pc | INT |

### Proxy

Set the http_proxy or HTTP_PROXY environment variable if you need to connect via proxy.
